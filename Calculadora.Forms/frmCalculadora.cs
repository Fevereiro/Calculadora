﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora.Forms
{
    public enum Operador
    {
        Soma, Subtracao, Divisao, Multiplicacao, RaizQuadrada
    }

    public partial class frmCalculadora : Form
    {
        Operador operador;

        public frmCalculadora()
        {
            InitializeComponent();

            this.AcceptButton = btnCalcular;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            var valores = txtDisplay.Text.Split('+', '-', '/', '*');

            decimal valor1 = Convert.ToDecimal(valores[0]);
            decimal valor2 = Convert.ToDecimal(valores[1]);

            var calculo =  CalculoFabrica.Criar(operador);
            var resultado = calculo.Calcular(valor1, valor2);

            ILog logServico = new LogDbServico();
            logServico.GravarLog(new Log { Data = DateTime.Now, Operacao = operador, Valor1 = valor1, Valor2 = valor2 });

            AjustarTextbox(resultado);
        }

        private void AjustarTextbox(decimal resultado)
        {
            txtDisplay.Text = resultado.ToString();
            txtDisplay.Focus();
            txtDisplay.SelectionStart = txtDisplay.Text.Length;
            txtDisplay.SelectionLength = 0;
        }

        private void txtDisplay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '/')
            {
                operador = Operador.Divisao;
            }

            if (e.KeyChar == '*')
            {
                operador = Operador.Multiplicacao;
            }

            if (e.KeyChar == '-')
            {
                operador = Operador.Subtracao;
            }

            if (e.KeyChar == '+')
            {
                operador = Operador.Soma;
            }
        }
    }
}
