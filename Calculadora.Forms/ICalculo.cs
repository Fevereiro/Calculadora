﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public interface ICalculo
    {
        decimal Calcular(decimal valor1, decimal valor2);
    }
}