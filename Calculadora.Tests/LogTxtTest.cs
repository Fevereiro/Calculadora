﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculadora.Forms;
using System.IO;
using NSubstitute;

namespace Calculadora.Tests
{
    /// <summary>
    /// Summary description for LogTextTest
    /// </summary>
    [TestClass]
    public class LogTxtTest
    { 
        [TestMethod]
        public void GravarLog_Arquivo_DeveGravarLogNoArquivo()
        {
            //Arrange
            var logServico = new LogTxtServico(new ArquivoServico());
            var log = new Log()
            {
                Id = 1,
                Data = DateTime.Now,
                Valor1 = 2,
                Valor2 = 1,
                Operacao = Operador.Soma
            };
            string path = LogTxtServico.logPath;

            //Act
            logServico.GravarLog(log);            
            string text = System.IO.File.ReadAllText(path);
            
            //Assert
            Assert.IsTrue(text.Contains($"[{log.Data}] [Operação: {log.Operacao.ToString()}] [Primeiro Valor: {log.Valor1.ToString()}] [Segundo Valor: { log.Valor2.ToString()}]"));                
        }

        [TestMethod]
        public void GravarLog_Arquivo_ArquivoFoiCriado()
        {
            //Arrange
            var logServico = new LogTxtServico(new ArquivoServico()); 
            var log = new Log()
            {
                Id = 1,
                Data = DateTime.Now,
                Valor1 = 2,
                Valor2 = 1,
                Operacao = Operador.Soma
            };
            string path = LogTxtServico.logPath;

            //Act
            logServico.GravarLog(log);

            //Assert
            Assert.IsTrue(File.Exists(path));
        }

        [TestMethod]
        public void GravarLog_MockandoArquivoClasseFake_LogDeveSerCriadoCorretamente()
        {
            //Arrange

            var mockArquivoServico = new MockArquivoServico();
            var logServico = new LogTxtServico(mockArquivoServico);
            var log = new Log()
            {
                Id = 1,
                Data = DateTime.Now,
                Valor1 = 2,
                Valor2 = 1,
                Operacao = Operador.Soma
            };

            //Act
            logServico.GravarLog(log);

            //Assert
            Assert.AreEqual($"[{log.Data}] [Operação: {log.Operacao.ToString()}] [Primeiro Valor: {log.Valor1.ToString()}] [Segundo Valor: { log.Valor2.ToString()}]", mockArquivoServico.logRecebido);
        }

        [TestMethod]
        public void GravarLog_MockandoArquivoNSubstitute_LogDeveSerCriadoCorretamente()
        {
            //Arrange
            var log = new Log()
            {
                Id = 1,
                Data = DateTime.Now,
                Valor1 = 2,
                Valor2 = 1,
                Operacao = Operador.Soma
            };

            string textoEsperado = $"[{log.Data}] [Operação: {log.Operacao.ToString()}] [Primeiro Valor: {log.Valor1.ToString()}] [Segundo Valor: { log.Valor2.ToString()}]";
            var mockArquivoServico = Substitute.For<IArquivoServico>();

            var logServico = new LogTxtServico(mockArquivoServico);

            //Act
            logServico.GravarLog(log);

            //Assert
            mockArquivoServico.Received().Salvar(Arg.Is<string>(textoEsperado), Arg.Any<string>());
        }

        [TestMethod]
        public void GravarLog_MockandoArquivoNSubstitute_PathFoiPreenchido()
        {
            //Arrange
            var log = new Log()
            {
                Id = 1,
                Data = DateTime.Now,
                Valor1 = 2,
                Valor2 = 1,
                Operacao = Operador.Soma
            };

            var mockArquivoServico = Substitute.For<IArquivoServico>();

            var logServico = new LogTxtServico(mockArquivoServico);

            //Act
            logServico.GravarLog(log);

            //Assert
            mockArquivoServico.Received().Salvar(Arg.Any<string>(), Arg.Is(LogTxtServico.logPath));
        }
    }
    public class MockArquivoServico : IArquivoServico
    {
        public string logRecebido;
        public void Salvar(string texto, string path)
        {
            logRecebido = texto;
        }
    }
}
